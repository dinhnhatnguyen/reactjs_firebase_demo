import "./css/App.css";

import Write from "./component/Write";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Read from "./component/Read";
import UpdateRead from "./component/UpdateRead";
import UpdateWrite from "./component/UpdateWrite";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Write />} />
          <Route path="/write" element={<Write />} />
          <Route path="/read" element={<Read />} />
          <Route path="/updateread" element={<UpdateRead />} />
          <Route path="/updatewrite/:firebaseId" element={<UpdateWrite />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
